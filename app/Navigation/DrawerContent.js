import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';
// import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function DrawerContent({navigation}){
    return(
        <View style={styles.container}>
            <View style={styles.drawerProfile}>
                <Image
                    style={styles.drawerProfileImage} 
                    source={{ uri:"https://picsum.photos/seed/picsum/200/300" }}
                />
                <Text style={styles.drawerProfileText}>Lorem Picsum</Text>
            </View>
            <View style={styles.drawerMenu}>  
                <TouchableOpacity style={styles.drawerMenuItem} onPress={()=>{navigation.navigate("Profile")}}  >
                    <Image style={styles.drawerMenuItemImage} source={{uri:"https://picsum.photos/200"}} />  
                    <Text style={styles.drawerMenuItemText}>
                        Profile
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerMenuItem} >
                    <Image style={styles.drawerMenuItemImage} source={{uri:"https://picsum.photos/200"}} />  
                    <Text style={styles.drawerMenuItemText}>
                        JGO Wallet
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerMenuItem} >   
                    <Image style={styles.drawerMenuItemImage} source={{uri:"https://picsum.photos/200"}} />  
                    <Text style={styles.drawerMenuItemText}>
                        Manual
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerMenuItem} >
                    <Image style={styles.drawerMenuItemImage} source={{uri:"https://picsum.photos/200"}} />  
                    <Text style={styles.drawerMenuItemText}>
                        Support
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.drawerMenuItem} >
                    <Image style={styles.drawerMenuItemImage} source={{uri:"https://picsum.photos/200"}} />  
                    <Text style={styles.drawerMenuItemText}>
                        Settings
                    </Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'#1c1a1a'
    },
    drawerProfile:{
        flex: 0.35,
        backgroundColor:"#db500b",
        borderBottomLeftRadius:30,
        borderBottomRightRadius:30,
        justifyContent:"space-evenly",
        alignItems:"center"
    },
    drawerProfileImage:{
        height:"40%",
        width:"45%",
        borderRadius:100
    },
    drawerProfileText:{
        fontSize:23,
        color:"white",
        fontWeight:"bold",
    },
    drawerMenu:{
        flex:0.65,
        alignItems:"center",
        justifyContent:"flex-start", 
    },
    drawerMenuItem:{
        width:"70%",
       marginTop:30,
       flexDirection:"row",
       justifyContent:"space-evenly"
    },
    drawerMenuItemText:{
        color:"#db500b",
        fontSize:17
    },
    drawerMenuItemImage:{
        height:50,
        width:50
    }


  });