import React from 'react';
// import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import LoginPage from '../modules/auth/LoginPage'
import RegisterPage from '../modules/auth/RegisterPage'
import HomePage from '../modules/home/HomePage'
import ProfilePage from '../modules/profile/ProfilePage'

import DrawerContent from './DrawerContent'


const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export const AuthNavigator = () => (
    <Stack.Navigator>
    <Stack.Screen
      name="Login"
      component={LoginPage}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Register"
      component={RegisterPage}
      options={{ headerShown: false }}
    />
    </Stack.Navigator>
  );

  export const HomeNavigator = () => (

      <Drawer.Navigator initialRouteName="Home" drawerContent={props => <DrawerContent {...props}  />} >
        <Drawer.Screen name="Home" component={HomePage} />
        <Drawer.Screen name="Profile" component={ProfilePage} options={{headerShown:true}} />
        <Drawer.Screen name="JGO Wallet" component={HomePage} />
        <Drawer.Screen name="Manual" component={HomePage} />
        <Drawer.Screen name="Support" component={HomePage} />
        <Drawer.Screen name="Settings" component={HomePage} />
        {/* <Drawer.Screen name="Notifications" component={NotificationsScreen} /> */}
      </Drawer.Navigator>

  );

  export default AppNavigator = () =>(
    <Stack.Navigator>
      <Stack.Screen
      name="Auth"
      component={AuthNavigator}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Home"
      component={HomeNavigator}
      options={{ headerShown: false }}
    />
    </Stack.Navigator>
  )
