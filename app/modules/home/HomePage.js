import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet,Dimensions, TouchableOpacity,ActivityIndicator  } from 'react-native';
import * as Location from 'expo-location';
import MapView,{Marker} from 'react-native-maps';

import AcceptJobModal from './AcceptJobModal'

export default function HomePage({navigation}) {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [loading,setLoading] = useState(true)
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        setLoading(false)
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
      setLoading(false)
    })();
  });

  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }

  

  if(location===null){
      return(
        <View style={{justifyContent:"center",alignItems:"center",flex:1}} >
          <ActivityIndicator/>
        </View>
      )
  }
  if(errorMsg===null){
    <View style={{justifyContent:"center",alignItems:"center",flex:1}} >
      <Text>{errorMsg}</Text>
    </View>
  }
  return (

    <View style={styles.container}>
        <MapView 
            style={styles.mapStyle}
            initialRegion={{
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                // latitude: 37.78825,
                // longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }}
            >
                <Marker
                coordinate={{
                    latitude: 37.78825,
                    longitude: -122.4324,
                }}
                 />
                
        </MapView>
        <TouchableOpacity style={styles.burgerMenu} onPress={()=>navigation.toggleDrawer()} />
        <TouchableOpacity style={styles.notificationMenu} onPress={()=>setModalVisible(!modalVisible)} />
        <AcceptJobModal
          modalVisible = {modalVisible}
          setModalVisible = {setModalVisible}

        />
        <View style={styles.details}>
                <View style={styles.detailsIcon}>
                
                </View>
                
                <View>
                    <Text style={styles.detailsNumbers} >
                        0.0%
                    </Text>
                    <Text style={styles.detailsText} >
                        Acceptance
                    </Text>
                </View>
                <View>
                    <Text style={styles.detailsNumbers} >
                        5.00
                    </Text>
                    <Text style={styles.detailsText} >
                        Ratings
                    </Text>
                </View>
                <View>
                    <Text style={styles.detailsNumbers} >
                        0.0%
                    </Text>
                    <Text style={styles.detailsText} >
                        Cancellation
                    </Text>
                </View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  details:{
    position:"absolute",
    bottom:0,
    height:"25%",
    width:"100%",
    backgroundColor:"rgba(0,0,0,0.8)",
    // opacity:0.8,
    borderTopLeftRadius:20,
    borderTopRightRadius:20,
    flex:1,
    justifyContent:"space-around",
    alignItems:"center",
    flexDirection:"row"
  },
  detailsNumbers:{
      fontSize:40,
      color:'orange'
  },
  detailsText:{
    fontSize:17,
    color:'white'
    },
  detailsIcon:{
    position:"absolute",
    height:"50%",
    width:"22%",
    backgroundColor:"rgba(0,0,0,1)",
    borderRadius:100,
    top:-50,
    left:"40%",
    opacity:2
      
  },
  burgerMenu:{
    height:30,
    width:30,
    borderRadius:100,
    backgroundColor:"#db500b",
    position:"absolute",
    top:"5%",
    left:"10%"
  },
  notificationMenu:{
    height:30,
    width:30,
    borderRadius:100,
    backgroundColor:"#db500b",
    position:"absolute",
    top:"5%",
    right:"10%"
  }

});