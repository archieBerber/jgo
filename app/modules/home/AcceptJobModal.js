import React, { Component, useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Image
} from "react-native";

const App = ({modalVisible,setModalVisible}) => {
  return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
            <View style={styles.modalView}>
                <View style={styles.modalViewHead}>
                  <Text style={styles.modalText1}>1.35Km</Text>
                  <Text style={styles.modalText2}>Php 103</Text>
                  <Text style={styles.modalText3}>CASH </Text>
                </View>
                <View style={styles.modalViewBody} >
                  <Text style={styles.modalViewBodyText1} >
                    CYPRESS GARDEN BLDG.
                  </Text>
                  <Text style={styles.modalViewBodyText2} >
                    12, 1229 V.A Rufino St, Legazpi Village,
                    Makati, 1229 Metro Manila
                  </Text>
                  <Image
                    style={styles.modalViewBodyImg} 
                    source={{ uri:"https://picsum.photos/seed/picsum/200/300" }} 
                  />
                  <Text style={styles.modalViewBodyText3} >
                    Additional Services
                  </Text>
                  <Text style={styles.modalViewBodyText4} >
                    None
                  </Text>
                  
                </View>
                <View style={styles.modalViewFooter}  >
                  <View style={styles.modalViewFooterArrow} >
                  </View>
                  <Text style={styles.modalViewFooterText1} >
                    JAZZ RESIDENCES
                  </Text>
                  <Text style={styles.modalViewFooterText2} >
                    Unit 3304 Tower B, Jupiter Corner
                    Nicanor Garcia Streets,
                    Barangay Bel-Air
                    Makati,1209 Manila, Philippines
                  </Text>
                </View>
            </View>
            <TouchableOpacity style={styles.modalButton} onPress={()=>setModalVisible(!modalVisible)} >
                <Text style={styles.modalButtonText}>Accept Delivery</Text>  
            </TouchableOpacity>
      </Modal>
  );
};

const styles = StyleSheet.create({
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    // padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height:"75%"

  },
  modalViewHead:{
    height:"10%",
    width:"100%",
    backgroundColor:"#e1e3e6",
    borderTopLeftRadius:20,
    borderTopRightRadius:20,
    justifyContent:"space-evenly",
    alignItems:"center",
    flexDirection:"row"


  },
  modalText1: {
    color:"#db500b",
    fontSize:17
    
  },
  modalText2: {
    color:"#db500b",
    fontSize:22,
    fontWeight:"bold"
   
  },
  modalText3: {
    color:"#db500b",
    fontSize:17
    
  },
  modalViewBody:{
    height:"55%",
    width:"100%",
    backgroundColor:"white",
    justifyContent:"center",
    alignItems:"center",


  },
  modalViewBodyImg:{
    height:"20%",
    width:"20%",
    marginVertical:10

  },
  modalViewBodyText1:{
    color:"#db500b",
    fontSize:21,
    fontWeight:"bold",
    marginVertical:10
 
  },
  modalViewBodyText2:{
    fontSize:18,
    textAlign:"center",
    paddingHorizontal:20,
    marginVertical:10
 
  },
  modalViewBodyText3:{
    color:"#db500b",
    fontSize:18,
    fontWeight:"bold",
    marginVertical:10
 
  },
  modalViewBodyText4:{
    fontSize:16,
    marginVertical:10
 
  },
  
  modalViewFooter:{
    height:"35%",
    width:"100%",
    backgroundColor:"#e1e3e6",
    justifyContent:"center",
    alignItems:"center",
    borderBottomLeftRadius:20,
    borderBottomRightRadius:20,
  },
  modalViewFooterText1:{
    color:"#db500b",
    fontSize:21,
    fontWeight:"bold"
  },
  modalViewFooterText2:{
    marginTop:20,
    fontSize:17,
    paddingHorizontal:50,
    textAlign:"center",
    // letterSpacing:1
  },

  modalViewFooterArrow:{
    height:50,
    width:50,
    borderRadius:100,
    backgroundColor:"white",
    position:"absolute",
    top:"-10%"
  },
  modalButton: {
    backgroundColor: "#db500b",
    borderRadius: 10    ,
    // padding: 35,
    alignItems: "center",
    justifyContent:"center",
    alignSelf:"center",
    height:"10%",
    width:"90%"

  },
  modalButtonText: {
    textAlign: "center",
    color:"white",
    fontSize:25

  }
  
});

export default App;