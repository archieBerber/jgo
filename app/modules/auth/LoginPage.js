import React from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import * as Yup from "yup";

import Button from "../../components/Button";
import Text from "../../components/Text";
import { Form, FormField, SubmitButton } from "../../components/forms";
import colors from "../../config/colors";
import Icon from "../../components/Icon";

const validationSchema = Yup.object().shape({
  // email: Yup.string().required().email().label("Email"),
  // password: Yup.string().required().min(8).label("Password"),
});

const LoginPage = ({ navigation }) => {

  const handleLogin = () =>{
    navigation.navigate('Home')
  }

  return (
    <View style={styles.container}>
      <ImageBackground
        style={styles.iconContainer}
        source={require("../../../assets/iconbg.jpg")}
      >
        <Image
          style={{ width: "100%", height: "100%" }}
          source={require("../../../assets/Jgo_logo_edit.png")}
        />
      </ImageBackground>
      <View style={styles.formContainer}>
        <Form
          initialValues={{ email: "", password: "" }}
          onSubmit={(values) => handleLogin(values)}
          validationSchema={validationSchema}
        >
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon="email"
            keyboardType="email-address"
            name="email"
            placeholder="E-mail"
            textContentType="emailAddress"
            style={{color:"white"}}
          />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            icon="lock"
            name="password"
            placeholder="Password"
            secureTextEntry
            textContentType="password"
            style={{color:"white"}}
          />
        
            <View style={styles.loginContainer}>
              <SubmitButton title="LOGIN" />
            </View>

          {/* <Text style={styles.loginWith}>Login with</Text>
          <View style={styles.socialContainer}>
            <Button title="Facebook" color="facebook" icon="facebook" />
            <View style={styles.socialSeparator} />
            <Button title="Google" color="google" icon="google" />
          </View> */}
          {/* <View style={styles.signupContainer}>
            <Text style={styles.noAccount}>Don't have an account?</Text>
            <TouchableOpacity
              style={styles.signupContainer}
              onPress={() => {
                navigation.navigate("Register");
              }}
            >
              <Text style={styles.signUp}>Sign up</Text>
            </TouchableOpacity>
          </View> */}
        </Form>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
  },
  iconContainer: {
    flex: 0.60,
  },
  formContainer: {
    flex: 0.40,
    backgroundColor: "#2e2e2e",
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    justifyContent:"center"
  },
  loginContainer: {
    width: "40%",
    marginHorizontal: 20,
    marginLeft: "15%",
  },
  forgotContainer: {
    flexDirection: "row",
    marginLeft: "12%",
    alignItems: "center",
  },
  forgot: {
    fontSize: 18,
    color: "#BC522F",
    textDecorationLine: "underline",
    fontWeight: "700",
  },
  noAccount: {
    fontSize: 15,
    color: colors.white,
    alignSelf: "center",
  },
  loginWith: {
    fontSize: 15,
    color: colors.white,
    alignSelf: "center",
  },
  signupContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  socialContainer: {
    flex: 1,
    width: "35%",
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "center",
    margin: 35,
  },
  signUp: {
    fontSize: 20,
    color: "#BC522F",
    textDecorationLine: "underline",
    fontWeight: "700",
    alignSelf: "center",
  },
  socialSeparator: {
    height: "100%",
    width: 2,
    marginHorizontal: 15,
    backgroundColor: colors.white,
  },
});

export default LoginPage;
