import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';

const ProfilePage = () => {
    return ( 
        <View style={styles.container}>
            <View style={styles.profileContainer}>  
                <View style={styles.profile}>  
                    <Image
                        style={styles.profileImage} 
                        source={{ uri:"https://picsum.photos/seed/picsum/200/300" }}
                    />
                    <Text style={styles.profileName} >Lorem Picsum</Text>
                    <Text style={styles.profileDriver} >JGO Driver</Text>
                </View>
            </View>
            <View style={styles.profileDetails} >
                <View>
                    <Text style={styles.detailsHeaderText} >Referal Code</Text>
                    <Text style={styles.detailsBodyText}>72aeicsnha213007</Text>
                </View>
                <View style={styles.detailsContainer}>
                    <View>
                        <Text style={styles.detailsHeaderText} >Total Delivery</Text>
                        <Text style={styles.detailsBodyText2}>12</Text>
                    </View>
                    <View>
                        <Text style={styles.detailsHeaderText} >Total Earn(Php)</Text>
                        <Text style={styles.detailsBodyText2}>1,920.00</Text>
                    </View>
                </View>
            </View>
        </View>
     );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'black'
    },
    profileContainer:{
        // backgroundColor:"#db500b",
        justifyContent:"center",
        alignItems:"center",
        flex:0.5
    },
    profile:{
        backgroundColor:"#db500b",
        // justifyContent:"center",
        alignItems:"center",
        height:"80%",
        width:"80%",
        borderTopLeftRadius:20,
        borderTopRightRadius:20,
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20,
        position:"relative"

    },
    profileImage:{
        height:"50%",
        width:"50%",
        borderRadius:100,
        position:"relative",
        top:"10%"
    },
    profileName:{
        fontSize:28,
        fontWeight:"bold",
        position:"relative",
        color:"white",
        top:"18%",
    },
    profileDriver:{
        fontSize:23,
        position:"relative",
        color:"white",
        top:"20%",
    },profileDetails:{
        justifyContent:"space-evenly",
        alignItems:"center",
        flex:0.4,

    },
    detailsHeaderText:{
        color:"#db500b",
        fontSize:19,
        fontWeight:"bold",
        marginBottom:20,
        textAlign:"center"

    },
    detailsBodyText:{
        color:"white",
        fontSize:25,
        textAlign:"center"

    },
    detailsBodyText2:{
        color:"white",
        fontSize:40,
        textAlign:"center"

    },
    detailsContainer:{
        flexDirection:"row",
        justifyContent:"space-evenly",
        width:"100%"
    },
    

  });
 
export default ProfilePage;