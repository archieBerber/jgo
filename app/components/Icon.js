import React from "react";
import { View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

function Icon({
  name,
  size = 40,
  backgroundColor = "#000",
  iconColor = "#FAFAFA",
}) {
  return (
    <View
      style={{
        width: size,
        height: size,
        backgroundColor,
        justifyContent: "center",
        alignItems: "center",
        borderBottomLeftRadius: 13,
        borderTopRightRadius: 13,
        backgroundColor: "#1D2B3B",
        borderWidth: 1,
        borderColor: "#FAFAFA",
      }}
    >
      <MaterialCommunityIcons name={name} color={iconColor} size={size * 0.5} />
    </View>
  );
}

export default Icon;
