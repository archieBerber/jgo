import React from "react";
import { View, StyleSheet, TouchableHighlight } from "react-native";
import Swipeable from "react-native-gesture-handler/Swipeable";

import Text from "../Text";
import colors from "../../config/colors";

function ListItem({ title, subTitle, onPress, renderRightActions }) {
  return (
    <Swipeable
      renderRightActions={renderRightActions}
      containerStyle={styles.swipeable}
    >
      <TouchableHighlight underlayColor={colors.primary} onPress={onPress}>
        <View style={styles.container}>
          <View style={styles.statusIndicator} />
          <View style={styles.detailsContainer}>
            <Text style={styles.title} numberOfLines={1}>
              {title}
            </Text>
            {subTitle && (
              <Text style={styles.subTitle} numberOfLines={1}>
                {subTitle}
              </Text>
            )}
          </View>
        </View>
      </TouchableHighlight>
    </Swipeable>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    padding: 12,
    backgroundColor: "#0C141D",
    borderRadius: 10,
    marginVertical: 6,
    width: "95%",
    alignSelf: "center",
  },
  detailsContainer: {
    flex: 1,
    marginLeft: 10,
    justifyContent: "center",
  },
  statusIndicator: {
    height: 30,
    width: 5,
    marginLeft: "-3%",
    backgroundColor: "#3BAEB3", //successful=#CE7C51, others=#3BAEB3, failed=#CE5151
  },
  subTitle: {
    color: colors.white,
    fontSize: 15,
  },
  swipeable: {
    backgroundColor: "#213447",
  },
  title: {
    fontWeight: "bold",
    color: colors.white,
    fontSize: 15,
  },
});

export default ListItem;
