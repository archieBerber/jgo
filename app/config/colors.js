export default {
  primary: "#e05a00",
  secondary: "#CC5121",
  black: "#000",
  white: "#FAFAFA",
  medium: "#6e6969",
  light: "#f8f4f4",
  dark: "#0c0c0c",
  danger: "#ff5252",
  facebook: "#4267b2",
  google: "#DB4437",
};
