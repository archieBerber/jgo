import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Platform } from 'react-native';

// Navigation
import { NavigationContainer } from "@react-navigation/native";
import AppNavigator,{AuthNavigator,HomeNavigator} from './app/Navigation/AppNavigator'
   


export default function App() {
  return (
    <NavigationContainer>
      <AppNavigator />
    </NavigationContainer>
    
  );
}


